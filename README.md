# TKPhotos #

### iOS App made in Swift that allows to maintain a local copy of pictures ###

## UI Features ##
* Custom Segmented Control
* Custom Image Picker
* Custom Camera Viewer
* Custom Container Controller
* Realm Database

## Thanks to ##

* [RealmSwift](https://github.com/realm/realm-cocoa)
* [JGProgressHUD](https://github.com/JonasGessner/JGProgressHUD)
* [BetterSegmentedControl](https://github.com/gmarm/BetterSegmentedControl)