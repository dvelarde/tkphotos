//
//  CameraViewController.swift
//  TKPhotos
//
//  Created by David Velarde on 8/28/16.
//  Copyright © 2016 David Velarde. All rights reserved.
//

import UIKit
import AVFoundation

class CameraViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    
    var session: AVCaptureSession?
    var device: AVCaptureDevice?
    var input: AVCaptureDeviceInput?
    var output = AVCaptureStillImageOutput()
    var prevLayer: AVCaptureVideoPreviewLayer?
    var pictureTaken : UIImage?
    @IBOutlet weak var cameraView: UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        createSession()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        prevLayer?.frame.size = cameraView.frame.size
    }
    
    func createSession() {
        session = AVCaptureSession()
        device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        
        do{
            self.input = try AVCaptureDeviceInput(device: self.device)
        }
        catch _
        {
            return
        }
        
        session?.addInput(input)
        output.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
        if session!.canAddOutput(output) {
            session!.addOutput(output)
        }
        
        prevLayer = AVCaptureVideoPreviewLayer(session: session)
        prevLayer?.frame.size = cameraView.frame.size
        prevLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        prevLayer?.connection.videoOrientation = transformOrientation(UIInterfaceOrientation(rawValue: UIApplication.sharedApplication().statusBarOrientation.rawValue)!)
        
        cameraView.addGestureRecognizer(UITapGestureRecognizer(target: self, action:"takePicture:"))
        cameraView.layer.addSublayer(prevLayer!)
        
        session?.startRunning()
    }
    
    func cameraWithPosition(position: AVCaptureDevicePosition) -> AVCaptureDevice? {
        let devices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
        for device in devices {
            if device.position == position {
                return device as? AVCaptureDevice
            }
        }
        return nil
    }

    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animateAlongsideTransition({ (context) -> Void in
            self.prevLayer?.connection.videoOrientation = self.transformOrientation(UIInterfaceOrientation(rawValue: UIApplication.sharedApplication().statusBarOrientation.rawValue)!)
            self.prevLayer?.frame.size = self.cameraView.frame.size
            }, completion: { (context) -> Void in
                
        })
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }
    
    func transformOrientation(orientation: UIInterfaceOrientation) -> AVCaptureVideoOrientation {
        switch orientation {
        case .LandscapeLeft:
            return .LandscapeLeft
        case .LandscapeRight:
            return .LandscapeRight
        case .PortraitUpsideDown:
            return .PortraitUpsideDown
        default:
            return .Portrait
        }
    }
    
    @IBAction func switchCameraSide(sender: AnyObject) {
        if let sess = session {
            let currentCameraInput: AVCaptureInput = sess.inputs[0] as! AVCaptureInput
            sess.removeInput(currentCameraInput)
            var newCamera: AVCaptureDevice
            if (currentCameraInput as! AVCaptureDeviceInput).device.position == .Back {
                newCamera = self.cameraWithPosition(.Front)!
            } else {
                newCamera = self.cameraWithPosition(.Back)!
            }
            var newVideoInput : AVCaptureDeviceInput?
            do{
                newVideoInput = try AVCaptureDeviceInput(device: newCamera)
            }
            catch _ {
                return
            }
            session?.addInput(newVideoInput)
        }
    }
    

    func takePicture(sender: UITapGestureRecognizer) {
        if let videoConnection = output.connectionWithMediaType(AVMediaTypeVideo) {
            output.captureStillImageAsynchronouslyFromConnection(videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                self.pictureTaken = UIImage(data: imageData)!
                self.performSegueWithIdentifier("fromCameraToPhotoDetail", sender: nil)
                
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let detailVC = segue.destinationViewController as? PhotoDetailViewController{
            detailVC.picture = self.pictureTaken;
            detailVC.sourceType = TKDetailType.FromCamera
        }
        
    }


}
