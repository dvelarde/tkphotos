//
//  PhotoDetailViewController.swift
//  TKPhotos
//
//  Created by David Velarde on 8/28/16.
//  Copyright © 2016 David Velarde. All rights reserved.
//

import UIKit

enum TKDetailType {
    case FromCamera
    case FromLibrary
    case FromMain
}

class PhotoDetailViewController: UIViewController,UIScrollViewDelegate {

    var picture : UIImage?
    var pictureName : String?
    var sourceType : TKDetailType?
    
    
    @IBOutlet weak var imgPicture: UIImageView!
    @IBOutlet weak var cancelPicture: UIButton!
    @IBOutlet weak var acceptPicture: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblPictureName: UILabel!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    
    var nameField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.minimumZoomScale = 1.0;
        self.scrollView.maximumZoomScale = 6.0;
        self.title = "Photo";
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        if let image = picture{
            self.imgPicture.image = image
        }
        else
        {
            self.navigationController?.popViewControllerAnimated(true)
        }
        
        if(sourceType! == .FromMain)
        {
            cancelPicture.hidden = true;
            acceptPicture.hidden = true;
            lblPictureName.hidden = false;
            imgBackground.hidden = false;
            btnBack.hidden = false;
            lblPictureName.text = pictureName!
            scrollView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action:"saveOrShare:"))
        }
        else
        {
            cancelPicture.hidden = false;
            acceptPicture.hidden = false;
            lblPictureName.hidden = true;
            imgBackground.hidden = true;
            btnBack.hidden = true;
            
        }
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - ScrollView Delegate
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView?
    {
        return self.imgPicture
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 
    
    func nameEntered(alert: UIAlertAction!){
        
        let savingResult = TKDataAccess.sharedInstance.saveNewImage(self.nameField!.text!, imageFile: picture!)
        if savingResult {
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        else{
            let saveImagePrompt = UIAlertController(title: "Error", message: "Sorry there was an error trying to save your image. Try again", preferredStyle: UIAlertControllerStyle.Alert)
            saveImagePrompt.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        }
    }
    func addTextField(textField: UITextField!){
        
        textField.placeholder = "Name"
        self.nameField = textField
    }

    @IBAction func tapOnAcceptPicture(sender: AnyObject) {
        let saveImagePrompt = UIAlertController(title: "Save Image", message: "Perfect! now you have to give this image a name so you can save it", preferredStyle: UIAlertControllerStyle.Alert)
        saveImagePrompt.addTextFieldWithConfigurationHandler(addTextField)
        saveImagePrompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive, handler: nil))
        saveImagePrompt.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nameEntered))
        presentViewController(saveImagePrompt, animated: true, completion: nil)
    }
    @IBAction func tapOnCloseModal(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func tapOnCancel(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func saveOrShare(sender : UILongPressGestureRecognizer)
    {
        let saveImagePrompt = UIAlertController(title: "Available Actions", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        saveImagePrompt.addAction(UIAlertAction(title: "Save to Camera Roll", style: UIAlertActionStyle.Default, handler: saveImage))
        saveImagePrompt.addAction(UIAlertAction(title: "Share External", style: UIAlertActionStyle.Default, handler: shareExternal))
        saveImagePrompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive, handler: nil))
        presentViewController(saveImagePrompt, animated: true, completion: nil)
    }
    
    func saveImage(alert: UIAlertAction!)
    {
        UIImageWriteToSavedPhotosAlbum(picture!, nil, nil, nil)
        let imageSavedPrompt = UIAlertController(title: "Success", message: "Your photo was saved", preferredStyle: UIAlertControllerStyle.Alert)
        imageSavedPrompt.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        presentViewController(imageSavedPrompt, animated: true, completion: nil)
    }
    func shareExternal(alert: UIAlertAction!)
    {
        let activityVC = UIActivityViewController(activityItems: [picture!], applicationActivities: nil)
        
        self.presentViewController(activityVC, animated: true, completion: nil)
        
        
    }
    
}
