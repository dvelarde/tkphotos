//
//  TKDataAccess.swift
//  TKPhotos
//
//  Created by David Velarde on 8/28/16.
//  Copyright © 2016 David Velarde. All rights reserved.
//

import Foundation
import RealmSwift


public class TKDataAccess{
    
    public class var sharedInstance: TKDataAccess {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: TKDataAccess? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = TKDataAccess()
        }
        return Static.instance!
    }
    
    let realm = try! Realm()
    
    private func getDocumentsURL() -> NSURL {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        return documentsURL
    }
    private func fileInDocumentsDirectory(filename: String) -> String {
        let fileURL = getDocumentsURL().URLByAppendingPathComponent(filename)
        return fileURL.path!
    }
    
    public func getAllImages() -> [TKPhoto]?
    {
        let result = realm.objects(TKPhoto.self)
        return Array(result)
    }
    
    public func getUIImageFromUdid(udid : String) -> UIImage?{
        let fileName = "\(udid).png";
        let imagePath = fileInDocumentsDirectory(fileName)
        if let image = UIImage(contentsOfFile: imagePath){
            return image
        }
        else{
            return nil
        }
    }
    
    
    public func saveNewImage(imageName: String, imageFile: UIImage) -> Bool{
        
        let uniqueIdentifier = NSUUID().UUIDString
        let fileName = "\(uniqueIdentifier).png";
        let newPicture = TKPhoto()
        
        newPicture.name = imageName
        newPicture.udid = fileName
        
        try! realm.write {
            realm.add(newPicture, update: false)
        }
        
        let imagePath = fileInDocumentsDirectory(fileName)
        
        let pngImageData = UIImagePNGRepresentation(imageFile)
        //let jpgImageData = UIImageJPEGRepresentation(image, 1.0)   // if you want to save as JPEG
        let result = pngImageData!.writeToFile(imagePath, atomically: true)
        
        return result
    }
    
}