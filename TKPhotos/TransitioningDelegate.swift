//
//  NavigationControllerDelegate.swift
//  TKPhotos
//
//  Created by David Velarde on 8/28/16.
//  Copyright © 2016 David Velarde. All rights reserved.
//

import UIKit

class TransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    
    var openingFrame: CGRect?
    var sourceImage : UIImage?
    
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let presentationAnimator = PresentationAnimator()
        presentationAnimator.openingFrame = openingFrame!
        presentationAnimator.sourceImage = sourceImage!
        return presentationAnimator
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let dismissAnimator = DismissalAnimator()
        dismissAnimator.openingFrame = openingFrame!
        dismissAnimator.sourceImage = sourceImage!
        return dismissAnimator
    }
    
}