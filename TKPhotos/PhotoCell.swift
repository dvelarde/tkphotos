//
//  PhotoCell.swift
//  TKPhotos
//
//  Created by David Velarde on 8/27/16.
//  Copyright © 2016 David Velarde. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var imgPhoto: UIImageView!
}
