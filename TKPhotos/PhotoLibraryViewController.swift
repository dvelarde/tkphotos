//
//  PhotoLibraryViewController.swift
//  TKPhotos
//
//  Created by David Velarde on 8/27/16.
//  Copyright © 2016 David Velarde. All rights reserved.
//

import UIKit
import Photos
import JGProgressHUD


class PhotoLibraryViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionPhotos: UICollectionView!
    
    var assets : PHFetchResult?
    var pictureSelected : UIImage?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        requestLibraryAuthorization()
        
        // Do any additional setup after loading the view.
    }
    
    func requestLibraryAuthorization(){
        
        PHPhotoLibrary.requestAuthorization({ auth in
            switch(auth)
            {
            case .Authorized:
                dispatch_async(dispatch_get_main_queue()) {
                    
                    self.reloadAssets()
                }
                
                
            default:
                
                let alertController = UIAlertController(title: "Permission Required", message: "We need access to your pictures so you can select those you want to add to TKPhotos.", preferredStyle: .Alert)
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
                    // ...
                }
                alertController.addAction(cancelAction)
                
                let OKAction = UIAlertAction(title: "Go to Settings", style: .Default) { (action) in
                    let url = NSURL(string:UIApplicationOpenSettingsURLString)
                    UIApplication.sharedApplication().openURL(url!)
                }
                
                alertController.addAction(OKAction)
                
                self.presentViewController(alertController, animated: true, completion: nil)

                
            }
        })
    }
    
    func reloadAssets()
    {
        let progress = JGProgressHUD(style: .ExtraLight)
        progress.textLabel.text = "Getting your pictures"
        progress.showInView(collectionPhotos)
        self.assets = PHAsset.fetchAssetsWithMediaType(PHAssetMediaType.Image, options: nil)
        self.collectionPhotos.reloadData()
        progress.dismiss()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.collectionPhotos.contentInset = UIEdgeInsetsZero;
        self.collectionPhotos.scrollIndicatorInsets = UIEdgeInsetsZero;
    }
    
    
    
    // MARK: - UICollectionViewDataSource
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let arrayPictures = assets
        {
            return arrayPictures.count
        }
        else{
            return 0;
        }
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let reuseIdentifier = "PhotoCell"
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath)
        cell.backgroundColor = .whiteColor()
        
        PHImageManager.defaultManager().requestImageForAsset(assets![indexPath.row] as! PHAsset, targetSize: CGSizeMake(cell.frame.size.width, cell.frame.size.height), contentMode: .AspectFill, options: nil) { (image: UIImage?, info: [NSObject : AnyObject]?) -> Void in
            (cell as! PhotoCell).imgPhoto.image = image
        }
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let progress = JGProgressHUD(style: .ExtraLight)
        progress.textLabel.text = "Getting picture in full size"
        progress.showInView(collectionPhotos)
        
        PHImageManager.defaultManager().requestImageForAsset(assets![indexPath.row] as! PHAsset, targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.Default, options: nil) { (image: UIImage?, info: [NSObject : AnyObject]?) -> Void in
            progress.dismiss()
            
            self.pictureSelected = image
            
            self.performSegueWithIdentifier("fromLibraryToPhotoDetail", sender: nil)
            
            
        }
        
        
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let screenSize = collectionView.bounds
        let desiredWidth : CGFloat = 100.0
        var desiredDivisions = NSInteger(screenSize.width / desiredWidth)
        if screenSize.width % desiredWidth != 0
        {
            desiredDivisions = desiredDivisions + 1
        }
        let divisions : CGFloat = CGFloat(desiredDivisions)
        let perfectSide = screenSize.width/divisions
        return CGSizeMake(perfectSide, perfectSide)
    }
    
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let detailVC = segue.destinationViewController as? PhotoDetailViewController{
            detailVC.picture = self.pictureSelected;
            detailVC.sourceType = TKDetailType.FromLibrary
        }
    }
    
    
    //    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    //        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    //    }
    
}
