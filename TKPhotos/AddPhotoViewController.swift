//
//  AddPhotoViewController.swift
//  TKPhotos
//
//  Created by David Velarde on 8/27/16.
//  Copyright © 2016 David Velarde. All rights reserved.
//

import UIKit
import BetterSegmentedControl


class AddPhotoViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    weak var currentViewController: UIViewController?
    var segmentedControlSelectedIndex : UInt = 0
    var control : BetterSegmentedControl?
    
    
    override func viewDidLoad() {
        
        self.currentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PhotoLibraryViewController")
        self.currentViewController!.view.translatesAutoresizingMaskIntoConstraints = false
        self.addChildViewController(self.currentViewController!)
        self.addSubview(self.currentViewController!.view, toView: self.containerView)
        super.viewDidLoad()
        
        self.title = "Library"
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        prepareCustomSegmentedControl()
        
        
    }
    
    func prepareCustomSegmentedControl(){
        
        if( control == nil){
        
            control = BetterSegmentedControl(
                frame: CGRect(x: 0.0, y: view.bounds.height - 44, width: view.bounds.width, height: 44.0),
                titles: ["Library","Camera"],
                index: self.segmentedControlSelectedIndex,
                backgroundColor: UIColor.darkGrayColor(),
                titleColor: .whiteColor(),
                indicatorViewBackgroundColor: UIColor.grayColor(),
                selectedTitleColor: .blackColor())
            
            control!.titleFont = UIFont(name: "HelveticaNeue", size: 14.0)!
            control!.selectedTitleFont = UIFont(name: "HelveticaNeue-Medium", size: 14.0)!
            control!.addTarget(self, action: #selector(controlValueChanged(_:)), forControlEvents: .ValueChanged)
        
            view.addSubview(control!)
        }
    }
    
    func controlValueChanged(sender: BetterSegmentedControl) {
        
        self.segmentedControlSelectedIndex = sender.index
        
        if self.segmentedControlSelectedIndex == 0 {
            
            let newViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PhotoLibraryViewController")
            newViewController!.view.translatesAutoresizingMaskIntoConstraints = false
            self.cycleFromViewController(self.currentViewController!, toViewController: newViewController!)
            self.currentViewController = newViewController
            self.title = "Library"
        } else {
            let newViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CameraViewController")
            newViewController!.view.translatesAutoresizingMaskIntoConstraints = false
            self.cycleFromViewController(self.currentViewController!, toViewController: newViewController!)
            self.currentViewController = newViewController
            self.title = "Camera"
        }
        
    }
    
    func addSubview(subView:UIView, toView parentView:UIView) {
        parentView.addSubview(subView)
        
        var viewBindingsDict = [String: AnyObject]()
        viewBindingsDict["subView"] = subView
        parentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[subView]|",
            options: [], metrics: nil, views: viewBindingsDict))
        parentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[subView]|",
            options: [], metrics: nil, views: viewBindingsDict))
    }
    
    func cycleFromViewController(oldViewController: UIViewController, toViewController newViewController: UIViewController) {
        oldViewController.willMoveToParentViewController(nil)
        self.addChildViewController(newViewController)
        self.addSubview(newViewController.view, toView:self.containerView!)
        newViewController.view.alpha = 0
        newViewController.view.layoutIfNeeded()
        UIView.animateWithDuration(0.5, animations: {
            newViewController.view.alpha = 1
            oldViewController.view.alpha = 0
            },
                                   completion: { finished in
                                    oldViewController.view.removeFromSuperview()
                                    oldViewController.removeFromParentViewController()
                                    newViewController.didMoveToParentViewController(self)
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
